package fahrzeug;

public class Main {
	
	private static String pkw1 = "Audi", pkw2 = "Mercedes", pkw3 = "VW", lkw1 = "Volvo", lkw2 = "VW";
	
	private static int sumPkws = 3, sumLkws = 2;
	private static int sumVehicles = sumPkws + sumLkws;
	
	private static double preis = 2000, rabatt = 10, rabattAudi = 5, discountEuros;
	private static boolean error = false;

	public static void main(String[] args) {

		for (int i = 1; i <= sumVehicles; i++) {
			System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl Fahrzeuge: " + i);
			if (i < sumVehicles)
				System.out.println("Es sind noch nicht alle Fahrzeuge registriert.");
			else
				System.out.println("Folgende Marken sind zu verkaufen: " + pkw1 + ", " + pkw2 + ", " + pkw3 + ", " + lkw1);
		}
		
		System.out.println("Es gibt " + (sumPkws != sumLkws ? "nicht " : "") + "gleich viele LKWs wie PKWs.");
		
		sumVehicles *= 2;
		System.out.println("Es gibt neue Fahrzeuge im Autohaus. Anzahl Fahrzeuge: " + sumVehicles);
		
		if (rabatt != 0)
			System.out.println("Rabatt: " + rabatt + "%");
		
		//pkw1 = "Mercedes";
		switch (pkw1) {
		
		case "Audi":
		case "Mercedes":
			rabatt = pkw1 == "Audi" ? rabattAudi : rabattAudi + 10;
			discountEuros = getDiscountInEur(preis, rabatt) * 5 / 2;
			
			if (discountEuros > getDiscountInEur(preis, 15))
				discountEuros = getDiscountInEur(preis, 15);
			break;
			
		case "VW":
		case "Volvo":
			preis = preis % 2 == 0 ? 1500 : 1700;
			discountEuros = 0;
			break;
			
		default:
			error = true;
		}
		
		if(!error)
			System.out.println("Der Preis des ersten PKWs ist: " + (preis - discountEuros));
		else
			System.out.println("Der Preis konnte nicht berechnet werden, da die Marke des Fahrzeugs nicht erkennbar ist.");

	}
	
	private static double getDiscountInEur(double price, double percentage) {
		return (price / 100) * percentage;
	}
	// comment to enable commit
}
