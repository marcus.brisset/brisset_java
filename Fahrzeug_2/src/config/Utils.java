package config;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {

    public static int getCurrentYear() {
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter dateYear = DateTimeFormatter.ofPattern("yyyy");
        String year = date.format(dateYear);

        return Integer.parseInt(year);
    }

    public static String trimIfExceeds(String string, int maxLength) {
        return (string.length() > maxLength) ? string.substring(0, maxLength - 3) + "..." : string;
    }

    public static void printMsg(String string) {
        System.out.println("╔═══╗\n║ ! ║ " + string + "\n╚═══╝");
    }

    public static void printMsg(String firstString, String secondString) {
        System.out.println("╔═══╗\n║ ! ║ " + firstString + "\n╚═══╝ " + secondString);
    }

}
