package config;

public class Config {

    public static final double
            PKW_DISCOUNT = 0.05,
            PKW_DISCOUNT_DISPLAYED = 0.03,
            PKW_DISCOUNT_MAX = 0.1,

            LKW_DISCOUNT = 0.06,
            LKW_DISCOUNT_MAX = 0.15,

            PRICE_UPPER_LIMIT = 2_147_483_647;

    public static final int
            DISPLAY_WIDTH = 50,
            LIST_PATTERN_3_COL3 = DISPLAY_WIDTH - 14,
            LIST_PATTERN_4_COL3 = 16,
            LIST_PATTERN_4_COL4 = DISPLAY_WIDTH - 29,
            SHOWROOM_LOTS = 11,
            SHOWROOM_EMPLOYEES = 3;

    public static final String
            SHOWROOM_NAME =      "Autohittn",
            TEXT_PATTERN =       "║ %-" + (DISPLAY_WIDTH - 2) + "s ║%n",
            MENU_PATTERN =       "║ %-3s %-" + (DISPLAY_WIDTH - 6) + "s ║%n",
            LIST_PATTERN_3 =     "║ %-4s %-6s %-" + LIST_PATTERN_3_COL3 + "s ║%n",
            LIST_PATTERN_4 =     "║ %-4s %-4s %-" + LIST_PATTERN_4_COL3 + "s %" + LIST_PATTERN_4_COL4 + "s ║%n",
            LIST_PATTERN_ALL_FIELDS = "║ %-4s %-4s %-" + LIST_PATTERN_4_COL3 + "s %-8s %-8s %13s ║%n";   // display width = 60
}
