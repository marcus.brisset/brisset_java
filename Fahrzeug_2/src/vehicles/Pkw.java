package vehicles;

import config.*;

public class Pkw extends Vehicle {

	private int yearLastDisplayed;

	public Pkw() {
		super(VehicleType.PKW);
	}

	public Pkw(String brand, int yearConstructed, int yearLastDisplayed, double price) {
		super(brand, yearConstructed, yearLastDisplayed, price);
	}

	@Override
	public double getDiscountAsFactor() {
		int yearsAlive = Utils.getCurrentYear() - super.getYearConstructed();
		int yearsDisplayed = this.yearLastDisplayed - super.getYearConstructed();
		double tempDiscount = (yearsAlive * Config.PKW_DISCOUNT) + (yearsDisplayed * Config.PKW_DISCOUNT_DISPLAYED);

		tempDiscount = (tempDiscount > Config.PKW_DISCOUNT_MAX) ? Config.PKW_DISCOUNT_MAX : (tempDiscount < 0) ? 0 : tempDiscount;
		return tempDiscount;
	}

	@Override
	public void print() {
		System.out.format(Config.LIST_PATTERN_ALL_FIELDS,
				super.getId(),
				this.getClass().getSimpleName().toUpperCase(),
				Utils.trimIfExceeds(super.getBrand(), Config.LIST_PATTERN_4_COL3),
				super.getYearConstructed(),
				this.yearLastDisplayed,
				formatDecimal.format(super.getPrice())
		);
	}

	public int getYearLastDisplayed() {
		return yearLastDisplayed;
	}

	public void setYearLastDisplayed(int newYear) {
		if (checkYearLastDisplayed(newYear)) {
			this.yearLastDisplayed = newYear;
		} else {
			Utils.printMsg(newYear + " is not a valid year. (The " + super.getBrand() + " was built in " + super.getYearConstructed() + ")");
		}
	}

	private boolean checkYearLastDisplayed(int year) {
		return year >= super.getYearConstructed() && year <= Utils.getCurrentYear();
	}

}