package vehicles;

import config.*;

public class Lkw extends Vehicle {

	public Lkw() {
		super(VehicleType.LKW);
	}

	public Lkw(String brand, int yearConstructed, double price) {
		super(brand, yearConstructed, price);
	}

	@Override
	public double getDiscountAsFactor() {
		int yearsAlive = Utils.getCurrentYear() - super.getYearConstructed();
		double discount = (yearsAlive * Config.LKW_DISCOUNT);
		
		discount = (discount > Config.LKW_DISCOUNT_MAX) ? Config.LKW_DISCOUNT_MAX : (discount < 0) ? 0 : discount;
		return discount;
	}

	@Override
	public void print() {
		System.out.format(Config.LIST_PATTERN_ALL_FIELDS,
				super.getId(),
				this.getClass().getSimpleName().toUpperCase(),
				Utils.trimIfExceeds(super.getBrand(), Config.LIST_PATTERN_4_COL3),
				super.getYearConstructed(),
				"",
				formatDecimal.format(super.getPrice())
		);
	}

}