package vehicles;

import config.Config;
import config.Utils;
import config.VehicleType;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public abstract class Vehicle {

    private int id, yearConstructed;
    private String brand;
    private double price;
    protected final DecimalFormat formatDecimal = new DecimalFormat("0.00");      // protected on purpose, so I don't have to import it twice for the subclasses
    private static int instanceCounter = 0;     // auto-incrementing id instead of asking for user input

    public Vehicle(String brand, int yearConstructed, int yearLastDisplayed, double price) {
        this.id = ++instanceCounter;
        this.brand = brand;
        this.yearConstructed = yearConstructed;
        this.price = Math.min(price, Config.PRICE_UPPER_LIMIT);
        ((Pkw) this).setYearLastDisplayed(yearLastDisplayed);
    }

    public Vehicle(String brand, int yearConstructed, double price) {
        this.id = ++instanceCounter;
        this.brand = brand;
        this.yearConstructed = yearConstructed;
        this.price = Math.min(price, Config.PRICE_UPPER_LIMIT);
    }

    public Vehicle(VehicleType vehicleType) {

        Scanner input = new Scanner(System.in);
        input.useLocale(Locale.US);
        int currentYear = Utils.getCurrentYear();
        int tempInputInt;
        double tempInputDbl;

        this.id = ++instanceCounter;
/*
        System.out.print("\nID: ");
        while (this.id < 1) {
            if (input.hasNextInt()) {
                tempInputInt = input.nextInt();
                if (tempInputInt > 0) {
                    this.id = tempInputInt;
                } else {
                    Utils.printMsg("Only positive numbers allowed!");
                    System.out.print("ID: ");
                }
            } else {
                Utils.printMsg("Only numbers allowed!");
                System.out.print("ID: ");
            }
            input.nextLine();
        }
*/

        System.out.print("Brand: ");
        brand = input.next();
        input.nextLine();

        System.out.print("Construction year: ");
        while (this.yearConstructed == 0) {
            if (input.hasNextInt()) {
                tempInputInt = input.nextInt();
                if (tempInputInt > 1885 && tempInputInt <= currentYear) {
                    this.yearConstructed = tempInputInt;
                } else {
                    Utils.printMsg("Invalid! " + (
                            tempInputInt > 1885 ?
                            "We currently have " + currentYear + "." :
                            "The first commercial car was built 1886."
                            )
                    );
                    System.out.print("Construction year: ");
                }
            } else {
                Utils.printMsg("Only numbers allowed!");
                System.out.print("Construction year: ");
            }
            input.nextLine();
        }

        if (vehicleType == VehicleType.PKW) {
            int yearLastDisplayed = 0;
            System.out.print("Year last displayed: ");
            while (yearLastDisplayed == 0) {
                if (input.hasNextInt()) {
                    tempInputInt = input.nextInt();
                    if (tempInputInt >= this.yearConstructed && tempInputInt <= currentYear) {
                        yearLastDisplayed = tempInputInt;
                        ((Pkw) this).setYearLastDisplayed(yearLastDisplayed);
                    } else {
                        Utils.printMsg("Invalid! " + (
                                tempInputInt > currentYear ?
                                "We currently have " + currentYear + "." :
                                "The " + brand + " was constructed in " + this.yearConstructed + "."
                                )
                        );
                        System.out.print("Year last displayed: ");
                    }
                } else {
                    Utils.printMsg("Only numbers allowed!");
                    System.out.print("Year last displayed: ");
                }
                input.nextLine();
            }
        }

        System.out.print("Price: ");
        while (this.price <= 0) {
            if (input.hasNextDouble()) {
                tempInputDbl = input.nextDouble();
                if (tempInputDbl > 0) {
                    if (tempInputDbl <= Config.PRICE_UPPER_LIMIT) {
                        this.price = tempInputDbl;
                    } else {
                        Utils.printMsg("Maximum price is " + formatDecimal.format(Config.PRICE_UPPER_LIMIT) + " EUR!");
                        this.price = 0;
                    }
                } else {
                    Utils.printMsg("Only positive numbers allowed!");
                    System.out.print("Price: ");
                }
            } else {
                Utils.printMsg("Only numbers allowed!");
                System.out.print("Price: ");
            }
            input.nextLine();
        }
        Utils.printMsg("Successfully added a " + this.brand + "!");

    }

    public abstract double getDiscountAsFactor();

    public abstract void print();

    public double getDiscountedPrice() {
        return this.price - this.price * getDiscountAsFactor();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYearConstructed() {
        return yearConstructed;
    }

    public void setYearConstructed(int yearConstructed) {
        this.yearConstructed = yearConstructed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
