package menu;

import config.Config;

import java.util.ArrayList;

public class Menu {

	private int size;
	private String header;
	private String
			headerPrefix = "[\s",
			headerSuffix = "\s]";
	
	private final ArrayList<Item> itemList = new ArrayList<>();

	public Menu(int size) {
		this.size = size;
	}

	public void addItem(String text) {
		this.itemList.add(new Item(text));
	}

	public void addItem(String key, String text1) {
		this.itemList.add(new Item(key, text1));
	}

	public void addItem(String key, String text1, String text2) {
		this.itemList.add(new Item(key, text1, text2));
	}

	public void addItem(String key, String text1, String text2, String text3) {
		this.itemList.add(new Item(key, text1, text2, text3));
	}

	public void addLine() {
		this.itemList.add(new Item("-".repeat(this.size - 2)));
	}

	public void printMenu() {
		printMenuHeader(this.header);
		for (Item item : itemList) {
			switch (item.getPattern()) {
				// Shorthand switch <3
				case Config.TEXT_PATTERN ->
						System.out.format(item.getPattern(), item.getText1());
				case Config.MENU_PATTERN ->
						System.out.format(item.getPattern(), item.getKey().isEmpty() ? "" : item.getKey() + ":", item.getText1());
				case Config.LIST_PATTERN_3 ->
						System.out.format(item.getPattern(), item.getKey(), item.getText1(), item.getText2());
				case Config.LIST_PATTERN_4 ->
						System.out.format(item.getPattern(), item.getKey(), item.getText1(), item.getText2(), item.getText3());
			}

		}
		printMenuFooter();
	}

	/*
		Places the title in the center of the header and wraps the ASCII signs around it,
		independent of MENU_WIDTH and title length. I'm so in love with it :D
		(Thanks to IntelliJ I'm now aware of the StringBuilder Class and String's repeat()
		method.)
	*/
	public void printMenuHeader(String title) {

		StringBuilder headerFiller = new StringBuilder();
		int postTitleSubtractor = 0;
		int halfWidth = (int) Math.ceil((double) this.size / 2) - (int) Math.ceil((double)title.length() / 2);
		boolean widthIsOdd = this.size % 2 != 0;
		boolean titleIsOdd = title.length() % 2 != 0;

		int titleAffixesLength = headerPrefix.length() + headerSuffix.length();

		if (title.length() > Config.DISPLAY_WIDTH - 1 - titleAffixesLength) {
			headerFiller.append(headerPrefix).append(title, 0, Config.DISPLAY_WIDTH - titleAffixesLength - 3).append("...").append(headerSuffix);
		} else if (title.isEmpty()) {
			headerFiller.append("═".repeat(this.size));
		} else {
			headerFiller.append("═".repeat(halfWidth - headerPrefix.length()));
			headerFiller.append(headerPrefix).append(title).append(headerSuffix);

			if (!widthIsOdd && titleIsOdd)
				postTitleSubtractor--;

			if (widthIsOdd && !titleIsOdd)
				postTitleSubtractor++;

			headerFiller.append("═".repeat(Math.max(halfWidth - postTitleSubtractor - headerSuffix.length(), 0)));
		}
		System.out.println("╔" + headerFiller + "╗");
	}

	public void printMenuHeader() {
		printMenuHeader(this.header);
	}

	public void printMenuFooter() {
		System.out.println("╚" + "═".repeat(this.size) + "╝");
	}

	public void printLine() {
		System.out.format(Config.TEXT_PATTERN, "-".repeat(this.size - 2));
	}

	public void clearItems() {
		this.itemList.clear();
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getHeaderPrefix() {
		return headerPrefix;
	}

	public void setHeaderPrefix(String headerPrefix) {
		this.headerPrefix = headerPrefix;
	}

	public String getHeaderSuffix() {
		return headerSuffix;
	}

	public void setHeaderSuffix(String headerSuffix) {
		this.headerSuffix = headerSuffix;
	}

	public ArrayList<Item> getItemList() {
		return itemList;
	}
}
