package menu;

import config.Config;

public class Item {

	private String key, text1, text2, text3, pattern;

	public Item(String text1) {
		this.text1 = text1;
		this.pattern = Config.TEXT_PATTERN;
	}

	public Item(String key, String text1) {
		this.key = key;
		this.text1 = text1;
		this.pattern = Config.MENU_PATTERN;
	}

	public Item(String key, String text1, String text2) {
		this.key = key;
		this.text1 = text1;
		this.text2 = text2;
		this.pattern = Config.LIST_PATTERN_3;
	}

	public Item(String key, String text1, String text2, String text3) {
		this.key = key;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.pattern = Config.LIST_PATTERN_4;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text) {
		this.text1 = text;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public String getText3() {
		return text3;
	}

	public void setText3(String text3) {
		this.text3 = text3;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}