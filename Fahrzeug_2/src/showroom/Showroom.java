package showroom;

import config.*;
import menu.Menu;
import vehicles.*;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Showroom {
	
	private final ArrayList<Vehicle> vehicleList;
	private final DecimalFormat formatDecimal = new DecimalFormat("0.00");

	public Showroom() {
		vehicleList = new ArrayList<>();
	}

	public void addVehicle(Vehicle vehicle) {
		vehicleList.add(vehicle);
	}

	public void printDemo() {
		Menu menu = new Menu(Config.DISPLAY_WIDTH);

		for (int i = 0; i < Config.DISPLAY_WIDTH - 1; i++) {
			menu.setHeaderPrefix("[" + i + "]\s");
			menu.setHeaderSuffix("\s[" + i + "]");
			menu.setHeader("X".repeat(i));
			menu.printMenu();
		}

		menu.setHeaderPrefix("[\s");
		menu.setHeaderSuffix("\s]");
		menu.setHeader("More Xs");
		for (int i = 0; i < Config.DISPLAY_WIDTH - 2; i++) {
			//menu.addItem(("-").repeat(i) + "X" + ("+").repeat(Config.DISPLAY_WIDTH - 2 - i));
			menu.addItem(("─").repeat(i) + "X");
			menu.addItem((" ").repeat(Config.DISPLAY_WIDTH - 3 - i) + "X" + ("─").repeat(i));
		}
		menu.printMenu();

		menu.clearItems();
		menu.setHeader("Even More Xs");
		for (int i = 0; i < 20; i++) {
			StringBuilder tempDump = new StringBuilder();
			for (int j = 0; j < Config.DISPLAY_WIDTH - 2; j++) {
				if (j % (i + 1) == 0) {
					tempDump.append("X");
				} else {
					tempDump.append("\s");
				}
			}
			menu.addItem(tempDump.toString());
		}
		menu.printMenu();

		menu.clearItems();
		menu.setHeader("What's the opposite of X? Riiight, X!");
		for (int i = 0; i < 20; i++) {
			StringBuilder tempDump = new StringBuilder();
			for (int j = 0; j < Config.DISPLAY_WIDTH - 2; j++) {
				if (j % (i + 1) != 0) {
					tempDump.append("X");
				} else {
					tempDump.append("\s");
				}
			}
			menu.addItem(tempDump.toString());
		}
		menu.printMenu();
	}

/*
	Prints all available brands, separated by commas (as was asked for).
	(Brand list is split into lines if its length exceeds display width.)
*/
	public void printAvailableBrands() {
		ArrayList<String> distinctList = new ArrayList<>();
		Menu menu = new Menu(Config.DISPLAY_WIDTH);

		menu.setHeader("Available Brands");

		for (Vehicle vehicle : vehicleList) {
			if (!distinctList.contains(vehicle.getBrand())) {
				distinctList.add(vehicle.getBrand());
			}
		}

		String distinctListAsStr = String.join(", ", distinctList);

		if (distinctListAsStr.length() > Config.DISPLAY_WIDTH - 2) {
			String[] distinctListSplitLines = distinctListAsStr.split("(?<=\\G.{" + (Config.DISPLAY_WIDTH - 2) + "})");
			for (String distinctListSplitLine : distinctListSplitLines) {
				menu.addItem(distinctListSplitLine);
			}
		} else {
			menu.addItem(distinctListAsStr);
		}
		menu.printMenu();
	}

	public void printPriceList() {
		if (!this.vehicleList.isEmpty()) {
			double priceTotal = 0;
			Menu menu = new Menu(Config.DISPLAY_WIDTH);

			menu.setHeader("Price List");
			menu.addItem("ID", "Type", "YOC\s\sBrand", "Price %");
			menu.addLine();

			for (Vehicle vehicle : this.vehicleList) {
				String vehicleBrand = vehicle.getYearConstructed() + "\s" + vehicle.getBrand();
				Utils.trimIfExceeds(vehicleBrand, 16);
				menu.addItem(vehicle.getId() + ":", vehicle.getClass().getSimpleName().toUpperCase(), Utils.trimIfExceeds(vehicleBrand, 16), formatDecimal.format(vehicle.getDiscountedPrice()) + " EUR");
				priceTotal += vehicle.getDiscountedPrice();
			}

			menu.addLine();
			menu.addItem("∑", String.valueOf(vehicleList.size()), "Vehicles", formatDecimal.format(priceTotal) + " EUR");
			menu.printMenu();
		} else {
			Utils.printMsg("No vehicles for sale.");
		}
	}

	public void printMatchingBrands() {
		ArrayList<String> matchList = new ArrayList<>();
		Menu menu = new Menu(Config.DISPLAY_WIDTH);

		menu.setHeader("Matching brands PKWs:LKWs");

		for (Vehicle vehicleA : this.vehicleList) {
			if (vehicleA instanceof Pkw) {
				for (Vehicle vehicleB : this.vehicleList) {
					if (vehicleB instanceof Lkw && vehicleA.getBrand().equals(vehicleB.getBrand()) && !matchList.contains(vehicleA.getBrand())) {
						matchList.add(vehicleA.getBrand());
						menu.addItem("Match for " + vehicleA.getBrand() + " found!");
					}
				}
			}
		}

		if (matchList.isEmpty()) {
			menu.addItem("No match found.");
		}

		menu.printMenu();
	}

	public void printVehicleBrandList(VehicleType type) {
		int counter = 0;
		Menu menu = new Menu(Config.DISPLAY_WIDTH);

		menu.setHeader("List of " + (type == VehicleType.PKW ? "PKWs" : "LKWs"));
		menu.addItem("Id", "YOC", "Brand");
		menu.addLine();
		for (Vehicle vehicle : this.vehicleList) {
			if (type == VehicleType.PKW && vehicle instanceof Pkw) {
				menu.addItem(vehicle.getId() + ":", String.valueOf(vehicle.getYearConstructed()), Utils.trimIfExceeds(vehicle.getBrand(), Config.LIST_PATTERN_3_COL3));
				counter++;
			} else if (type == VehicleType.LKW && vehicle instanceof Lkw) {
				menu.addItem(vehicle.getId() + ":", String.valueOf(vehicle.getYearConstructed()), vehicle.getBrand());
				counter++;
			}
		}

		if (counter == 0) menu.addItem("No " + (type == VehicleType.PKW ? "PKWs" : "LKWs") + " in the Showroom");
		menu.printMenu();
	}

	public void printOverloadStatus() {
		Menu menu = new Menu(Config.DISPLAY_WIDTH);

		menu.setHeader("Overload Status");
		menu.addItem("Current occupancy:");
		menu.addLine();
		menu.addItem("", String.valueOf(Config.SHOWROOM_LOTS), checkOverloadLots() ? "[!]\sLots (" + (getSumVehicles() - Config.SHOWROOM_LOTS) + " short)" : "\s".repeat(4) + "Lots");
		menu.addItem("", String.valueOf(Config.SHOWROOM_EMPLOYEES), checkOverloadEmployees() ? "[!] Employees (" + (int) (Math.ceil((float) getSumVehicles() / Config.SHOWROOM_EMPLOYEES) - Config.SHOWROOM_EMPLOYEES) + " short)" : "\s".repeat(4) + "Employees");
		menu.addItem("", String.valueOf(getSumVehicles()), "\s".repeat(4) + "Vehicles");
		menu.addLine();
		menu.addItem("Showroom " + (checkOverload() ? "IS" : "is NOT") + " overloaded.");
		menu.printMenu();
	}

	public boolean checkOverload() {
		return getSumVehicles() > Config.SHOWROOM_EMPLOYEES * 3 || getSumVehicles() > Config.SHOWROOM_LOTS;
	}

	public boolean checkOverloadEmployees() {
		return getSumVehicles() > Config.SHOWROOM_EMPLOYEES * 3;
	}

	public boolean checkOverloadLots() {
		return getSumVehicles() > Config.SHOWROOM_LOTS;
	}

	public ArrayList<String> getOverloadCause() {
		ArrayList<String> causality = new ArrayList<>();

		if (getSumVehicles() > Config.SHOWROOM_EMPLOYEES * 3)
			causality.add("Employees");
		if (getSumVehicles() > Config.SHOWROOM_LOTS)
			causality.add("Lots");

		return causality;
	}

	public void printName() {
		System.out.println("~~~ " + Config.SHOWROOM_NAME + " ~~~");
	}

	public int getSumVehicles() {
		return this.vehicleList.size();
	}

	public int getSumPkws() {
		int sumPkws = 0;
		if (!this.vehicleList.isEmpty()) {
			for (Vehicle vehicle : this.vehicleList) {
				if (vehicle instanceof Pkw) {
					sumPkws++;
				}
			}
		}
		return sumPkws;
	}

	public int getSumLkws() {
		int sumLkws = 0;
		if (!this.vehicleList.isEmpty()) {
			for (Vehicle vehicle : this.vehicleList) {
				if (vehicle instanceof Lkw) {
					sumLkws++;
				}
			}
		}
		return sumLkws;
	}

	public void printAllFields(){
		Menu menu = new Menu(60);
		menu.setHeader("All fields");
		menu.printMenuHeader();
		System.out.format(Config.LIST_PATTERN_ALL_FIELDS, "", "", "", "YEAR OF", "LAST", "PRICE");
		System.out.format(Config.LIST_PATTERN_ALL_FIELDS, "ID", "TYPE", "BRAND", "CONSTR.", "DISPLAY", "(EUR)");
		menu.printLine();
		for (Vehicle vehicle : vehicleList) {
			vehicle.print();
		}
		menu.printMenuFooter();
	}

	public void print() {
		for (Vehicle vehicle : vehicleList) {
			System.out.println(vehicle.getId());
			System.out.println(vehicle.getBrand());
			System.out.println(vehicle.getYearConstructed());
			if (vehicle instanceof Pkw) {
				System.out.println(((Pkw) vehicle).getYearLastDisplayed());
			}
			System.out.println(vehicle.getPrice());
		}
	}

}
