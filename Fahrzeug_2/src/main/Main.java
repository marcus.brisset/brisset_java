package main;

import config.*;
import menu.Menu;
import showroom.Showroom;
import vehicles.Lkw;
import vehicles.Pkw;

import java.util.Scanner;

public class Main {

	public static void main(String[] args)  {

		Showroom showroom = new Showroom();
		String choice = null;

		// Dummy data
		showroom.addVehicle(new Pkw("Volvo", 2000, 2005, 2000));
		showroom.addVehicle(new Pkw("Audi", 2017, 2020, 4875));
		showroom.addVehicle(new Pkw("BMW", 2020, 2021, 31500));
		showroom.addVehicle(new Pkw("Nissan", 2019, 2020, 7850.75));
		showroom.addVehicle(new Pkw("Audi", 2008, 2010, 17950));
		showroom.addVehicle(new Lkw("Iveco", 2000, 5000));
		showroom.addVehicle(new Lkw("Mercedes", 2010, 45000));
		showroom.addVehicle(new Lkw("Ford", 2001, 52500));
		showroom.addVehicle(new Lkw("GMC", 2019, 27499.90));

		Scanner scanner = new Scanner(System.in);
		do {
			Menu menu = new Menu(Config.DISPLAY_WIDTH);
			menu.setHeader("Showroom " + Config.SHOWROOM_NAME);

			boolean checkOverload = showroom.checkOverload();
			menu.addItem("(PKWs: " + showroom.getSumPkws() + " LKWs: " + showroom.getSumLkws()
					+ " LOTS: " + Config.SHOWROOM_LOTS + ")" + (!checkOverload ? "" : " OVERLOADED!")
			);
			if (checkOverload) {
				String causalities = String.join(" and ", showroom.getOverloadCause());
				menu.addItem("More " + causalities + " needed!");
			}

			menu.addLine();
			menu.addItem("1", "Add PKW");
			menu.addItem("2", "Add LKW");
			menu.addItem("3", "Show PKWs");
			menu.addItem("4", "Show LKWs");
			menu.addItem("5", "Show available brands");
			menu.addItem("6", "Show Price List");
			menu.addItem("7", "Match PKW & LKW brands");
			menu.addItem("8", "Show Overload Status");
			menu.addItem("q", "Quit");
			menu.addLine();
			menu.addItem("d", "Show Demo");
			menu.addItem("p", "Expose all Fields");
			menu.printMenu();
			System.out.print("root@" + Config.SHOWROOM_NAME.toLowerCase() + ":~# ");

			if (scanner.hasNext()) {
				choice = scanner.nextLine();

				switch (choice) {
					case "1":
						showroom.addVehicle(new Pkw());
						break;
					case "2":
						showroom.addVehicle(new Lkw());
						break;
					case "3":
						showroom.printVehicleBrandList(VehicleType.PKW);
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "4":
						showroom.printVehicleBrandList(VehicleType.LKW);
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "5":
						showroom.printAvailableBrands();
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "6":
						showroom.printPriceList();
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "7":
						showroom.printMatchingBrands();
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "8":
						showroom.printOverloadStatus();
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "d":
						showroom.printDemo();
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;
					case "p":
						showroom.printAllFields();
						System.out.println("(Hit enter to continue)");
						scanner.nextLine();
						break;

					case "q":
						break;
					default:
						Utils.printMsg("Please pick a valid choice.");
				}
			}
		} while (!("q").equals(choice));
		scanner.close();
	}
}